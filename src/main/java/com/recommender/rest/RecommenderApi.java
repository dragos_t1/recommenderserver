package com.recommender.rest;

import org.apache.commons.lang.StringUtils;
import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.common.FastByIDMap;
import org.apache.mahout.cf.taste.impl.model.*;
import org.apache.mahout.cf.taste.impl.recommender.svd.Factorizer;
import org.apache.mahout.cf.taste.impl.recommender.svd.SVDPlusPlusFactorizer;
import org.apache.mahout.cf.taste.impl.recommender.svd.SVDRecommender;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.model.Preference;
import org.apache.mahout.cf.taste.model.PreferenceArray;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.recommender.UserBasedRecommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;
import org.slf4j.Logger;

import static com.recommender.rest.Defs.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RecommenderApi {

    public static final MysqlConnector mc = new MysqlConnector();

    private static final java.util.logging.Logger LOG = java.util.logging.Logger.getLogger(RecommenderApi.class.getName());

    public RecommenderApi() throws Exception {
        mc.connect();
    }

    public boolean checkAuth(Request.Recommend req){

        Statement statement = null;
        try {
            statement = mc.getConn().createStatement();

        String sql = "SELECT * from apps where appkey = '" + req.apiKey +
                    "' and appsecret = '" + req.apiSecret + "'";
        ResultSet rs = statement.executeQuery(sql);
        if(rs.next()) {
            req.appId = rs.getInt("id");
            return true;
        }
        else
            return false;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Method that loops through all the apps and gathers all data
     */
    public void gatherData(){
        /*Statement statement = QueryBuilder.select()
                .all()
                .from(CASSANDRA_KEYSPACE, CASSANDRA_APP_TABLE);
        ResultSet results = cc.getSession().execute(statement);

        ExecutorService executor = Executors.newFixedThreadPool(Defs.NO_OF_THREADS);

        for(Row row : results.all()){
            LOG.info("Fetching data for " + row.getString("appId"));
            Runnable worker = new DataGatherer(row.getString("data_url"), LOG);
            executor.execute(worker);
        }

        executor.shutdown();*/
    }


    public List<Recommendation> getRecommendations(Request.Recommend req, int userId, int howMany) throws TasteException, SQLException {

        PreparedStatement preparedStmt = null;

        String sql = "SELECT c.id_category from app_categories c JOIN apps a ON a.id = c.id_app " +
                    "where a.appkey LIKE ?";
        preparedStmt = mc.getConn().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        preparedStmt.setString(1, req.apiKey);

        List<Integer> categories = new ArrayList<Integer>();

        ResultSet categoriesRs = preparedStmt.executeQuery();

        while(categoriesRs.next()){
            categories.add(categoriesRs.getInt("id_category"));
        }

        DataModel dataModel = buildModel(req, categories);

        Factorizer factorizer = new
                SVDPlusPlusFactorizer(dataModel,
                4,1000);
        Recommender recommender = new SVDRecommender
                (dataModel,factorizer);

/*
        UserSimilarity similarity = new PearsonCorrelationSimilarity(dataModel);
        UserNeighborhood neighborhood = new ThresholdUserNeighborhood(1, similarity, dataModel);
        UserBasedRecommender recommender = new GenericUserBasedRecommender(dataModel, neighborhood, similarity);
        */
        List<RecommendedItem> topItems = recommender
                .recommend(userId,howMany);

        List<Recommendation> recs = new ArrayList<Recommendation>();
        for(RecommendedItem ri : topItems){

            String sqlItem = "SELECT * from items where id = ?";
            PreparedStatement preparedStmtItems = mc.getConn().prepareStatement(sqlItem);
            preparedStmtItems.setInt(1, (int)ri.getItemID());

            ResultSet itemInfo = preparedStmtItems.executeQuery();

            String item_name = "";
            String url = "";
            String photo_url = "";
            while(itemInfo.next()){
                item_name = itemInfo.getString("name");
                url = itemInfo.getString("url");
                photo_url = itemInfo.getString("photo_url");
            }

            recs.add(new Recommendation(ri.getItemID(), ri.getValue(), item_name, url, photo_url ));
        }

        return recs;
    }

    private DataModel buildModel(Request.Recommend req, List<Integer> categories){

        try {
            String ratingType = "unary";
            String sql = "";
            String categoriesStr = StringUtils.join(categories.toArray(), ", ");

            int currentUserId = -1;

            FastByIDMap<PreferenceArray> userIdMap = new FastByIDMap<PreferenceArray>();

            Statement usersStmt = mc.getConn().createStatement();

            sql = "SELECT p.user_id, p.item_id from preferences p JOIN items i ON i.id = p.item_id " +
                    " WHERE i.category IN ( " + categoriesStr + " ) ";
            ResultSet allUsers = usersStmt.executeQuery(sql);
            while(allUsers.next()){
                int userId = allUsers.getInt("user_id");
                if(userId == currentUserId)
                    continue;
                currentUserId = userId;

                Statement prefStmt = mc.getConn().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                        ResultSet.CONCUR_READ_ONLY);

                sql = "SELECT p.user_id, p.item_id from preferences p JOIN items i ON i.id = p.item_id " +
                        " WHERE i.category IN ( " + categoriesStr + " ) AND p.user_id = " + userId;
                ResultSet prefs = prefStmt.executeQuery(sql);

                int size = 0;
                try {
                    prefs.last();
                    size = prefs.getRow();
                    prefs.beforeFirst();
                }
                catch(Exception ex) {
                }

                PreferenceArray usersPref = null;
                if(ratingType.equals("unary"))
                    usersPref = new BooleanUserPreferenceArray(size);
                else
                    usersPref = new GenericUserPreferenceArray(size);
                int idx = 0;

                while(prefs.next()){
                    int itemId = prefs.getInt("item_id");

                    Preference pref = null;
                    if(ratingType.equals("unary"))
                        pref = new BooleanPreference(userId,
                                itemId);
                    /*else
                        pref  = new GenericPreference(userId,
                                itemId, rating);*/

                    usersPref.set(idx, pref);
                    idx++;
                }

                userIdMap.put(userId, usersPref);
            }

            DataModel dataModel = new GenericDataModel(userIdMap);
            return dataModel;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Method that gets recommended categories for a given category
     */

    public void getRecommendedCategories(int categoryId, int howMany){

    }

    public int getUserId(String user_id) throws SQLException {
        Statement prefStmt = mc.getConn().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                ResultSet.CONCUR_READ_ONLY);
        int id = 0;
        String sql = "SELECT * from user_mappings where user_id LIKE '" + user_id + "'";
        ResultSet items = prefStmt.executeQuery(sql);
        boolean exists = false;
        while(items.next()){
            id = items.getInt("user_id_int");
            exists = true;
        }

        if(!exists){
            String query = " insert into user_mappings (user_id)"
                    + " values (?)";

            // create the mysql insert preparedstatement
            PreparedStatement preparedStmt = mc.getConn().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            preparedStmt.setString (1, user_id);
            id = preparedStmt.executeUpdate();
        }

        return id;
    }

    public int getItemId(String url, int category, String name, String photo) throws SQLException {
        Statement prefStmt = mc.getConn().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                ResultSet.CONCUR_READ_ONLY);

        int id = 0;
        String sql = "SELECT * from items where url LIKE '" + url + "%'";
        ResultSet items = prefStmt.executeQuery(sql);
        boolean exists = false;
        while(items.next()){
            id = items.getInt("id");
            exists = true;
        }

        if(!exists){
            String query = "INSERT INTO items (url, category, name, photo_url)"
                    + " values (?, ?, ?, ?)";

            // create the mysql insert preparedstatement
            PreparedStatement preparedStmt = mc.getConn().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            preparedStmt.setString (1, url);
            preparedStmt.setInt(2, category);
            preparedStmt.setString (3, name);
            preparedStmt.setString (4, photo);
            id = preparedStmt.executeUpdate();
        }

        return id;
    }

    public int getCategory(String category) throws SQLException {
        if(category == null)
            return 0;
        int id = 0;
        String sql = "";
        PreparedStatement preparedStmt = null;

        if(isNumeric(category)) {
            sql = "SELECT * from categories where id = ?";
            preparedStmt = mc.getConn().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparedStmt.setInt(1, Integer.parseInt(category));
        }
        else
        {
            sql = "SELECT * from categories where name LIKE ?";
            preparedStmt = mc.getConn().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparedStmt.setString(1, category);
        }
        ResultSet items = preparedStmt.executeQuery();
        boolean exists = false;

        while(items.next()){
            id = items.getInt("id");
            exists = true;
        }
        return exists ? id : 0;
    }

    public boolean addPref(int user_id, int item_id) throws SQLException {
        String sql = "INSERT INTO preferences (user_id, item_id) values (?, ?)";
        PreparedStatement preparedStmt = mc.getConn().prepareStatement(sql);
        preparedStmt.setInt(1, user_id);
        preparedStmt.setInt(2, item_id);
        return preparedStmt.execute();
    }

    public static boolean isNumeric(String str)
    {
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }

}
