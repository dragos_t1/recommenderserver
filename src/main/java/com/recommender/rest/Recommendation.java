package com.recommender.rest;

public class Recommendation {
    public long itemId;
    public double rating;
    public String item_name;
    public String url;
    public String photo_url;

    public Recommendation(long itemId, double rating, String item_name, String url, String photo_url){
        this.itemId = itemId;
        this.rating = rating;
        this.item_name = item_name;
        this.url = url;
        this.photo_url = photo_url;
    }
}
