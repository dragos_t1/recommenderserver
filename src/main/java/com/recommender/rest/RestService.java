package com.recommender.rest;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.model.*;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.codehaus.jackson.map.ObjectMapper;

import javax.annotation.PostConstruct;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("/api")
public class RestService {

    private static final Logger LOG = Logger.getLogger( RestService.class.getName() );

    RecommenderApi recommenderApi;
    ObjectMapper mapper = new ObjectMapper();

    public RestService() throws Exception {
        recommenderApi = new RecommenderApi();
    }

    @GET
    @Path("/recommend")
    @Produces(MediaType.APPLICATION_JSON)
    public Response recommend(@QueryParam("apiKey") String apiKey,
                              @QueryParam("apiSecret") String apiSecret,
                              @QueryParam("toUser") String toUser,
                              @QueryParam("howMany") int howMany) {


        List<Recommendation> recommendations = null;
        long  start = System.currentTimeMillis();
        Request.Recommend req = null;

        try {
            int toUserInt = recommenderApi.getUserId(toUser);
            req = new Request.Recommend(apiKey, apiSecret, toUserInt, howMany);

            if(!recommenderApi.checkAuth(req))
                return Response.status(403).entity("Not authorized").build();



            recommendations = recommenderApi.getRecommendations(req, req.toUser, req.howMany);
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Failed to get recommendation for " + req.toString());
        }
        long stop =  System.currentTimeMillis();

        LOG.log(Level.INFO, "Time spent: " + ((stop - start)));

        String response = "";

        try {
            response = mapper.writeValueAsString(recommendations);
        } catch (IOException e) {
            response = "error";
        }

        return Response.status(200).entity(response).build();

    }

    @POST
    @Path("/add-preference")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_PLAIN)
    public Response addPreference(@FormParam("url") String url,
                                  @FormParam("user_id") String user_id,
                                  @FormParam("category") String categoryStr,
                                  @FormParam("name") String name,
                                  @FormParam("photo") String photo){

        LOG.log(Level.INFO, "Primit "+url + " " + user_id);

        try {
            int category = recommenderApi.getCategory(categoryStr);
            if(category == 0) {
                LOG.log(Level.SEVERE, "Category doesn't exists!");
                return Response.status(200).entity("Category doesn't exists!").build();
            }
            int user_id_int = recommenderApi.getUserId(user_id);
            int item_id = recommenderApi.getItemId(url, category, name, photo);

            LOG.info(item_id + " " + user_id_int);

            recommenderApi.addPref(user_id_int, item_id);


        } catch (SQLException e) {
            LOG.log(Level.SEVERE, e.getMessage());
            return Response.status(403).entity("ERROR").build();
        }

        return Response.status(200).entity("Pref added!").build();

    }

}
