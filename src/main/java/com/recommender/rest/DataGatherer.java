package com.recommender.rest;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.util.logging.Level;
import java.util.logging.Logger;

public class DataGatherer implements Runnable {

    private final String url;
    private final Logger LOG;

    public DataGatherer(String url, Logger LOG){
        this.url = url;
        this.LOG = LOG;
    }

    @Override
    public void run() {
        try {
            String response = doGet();
        } catch (Exception e){
            LOG.log(Level.SEVERE, "Failed to get data from " + this.url + ":" + e.getMessage());
        }
    }

    private String doGet() throws Exception{
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(this.url);
        CloseableHttpResponse response1 = httpclient.execute(httpGet);
        HttpEntity entity1 = response1.getEntity();
        try {
            System.out.println(response1.getStatusLine());
            LOG.info("entity " + entity1.toString());
            EntityUtils.consume(entity1);
        } finally {
            response1.close();
        }

        return entity1.toString();
    }
}
