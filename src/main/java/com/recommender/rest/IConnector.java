package com.recommender.rest;

public interface IConnector {
    public void connect() throws Exception ;
}
