package com.recommender.rest;

public class Request {
    public static class Recommend{
        public String apiKey;
        public String apiSecret;
        public int toUser;
        public int howMany;
        public int appId;

        public Recommend(String apiKey, String apiSecret, int toUser, int howMany){
            this.apiKey = apiKey;
            this.apiSecret = apiSecret;
            this.toUser = toUser;
            this.howMany = howMany;
        }
    }
}
