package com.recommender.rest;

public class Defs {

    public static String MYSQL_HOST = "localhost";
    public static String MYSQL_PORT = "3306";
    public static String MYSQL_USER = "root";
    public static String MYSQL_PASS = "";
    public static String MYSQL_DB = "advisr";

    public static String CASSANDRA_HOST = "localhost";
    public static int CASSANDRA_PORT = 9042;

    public static String CASSANDRA_KEYSPACE = "advisr";
    public static String APP_TABLE = "apps";
    public static String PREFERENCES_TABLE = "preferences";
    public static String CASSANDRA_USER_MAPPINGS_TABLE = "user_mappings";
    public static String CASSANDRA_ITEM_MAPPINGS_TABLE = "item_mappings";

    public static String ADMIN_TOKEN = "ASDASD34gfdDF54gfdHFGH878768F";
    public static int NO_OF_THREADS = 10;
}
