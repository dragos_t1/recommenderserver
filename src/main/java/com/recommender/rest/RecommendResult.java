package com.recommender.rest;

public class RecommendResult {
    public int id;
    public float value;
    public int category;
}
